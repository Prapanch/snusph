#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "kernel.h"
#include "boundary.h"
#include "sph.h"
#include "run.h"
#include "forces.h"

static void allocate_ghost_particles(int g_N);
void boundary_condition(particleData *pdata, int check)
{
  int i;
//printf("\n\n\n \t %d \n\n",check);
#ifdef _OPENMP
#pragma omp parallel for default(shared) private(i) 
#endif
  for (i=0;i<N;i++){

/*    if(check%2 == 0){
      pdata[i].vel.x = -100.0*pdata[i].pos.x;
      pdata[i].vel.y = 100.0*pdata[i].pos.y;
      pdata[i].veln.x = -100.0*pdata[i].pos.x;
      pdata[i].veln.y = 100.0*pdata[i].pos.y;
    }else{
      pdata[i].vel.x = 100.0*pdata[i].pos.x;
      pdata[i].vel.y = -100.0*pdata[i].pos.y;
      pdata[i].veln.x = 100.0*pdata[i].pos.x;
      pdata[i].veln.y = -100.0*pdata[i].pos.y;

    }
*/
/*
    if(pdata[i].pos.x > xhigh->x-DELTA){
      pdata[i].vel.x = 0.0;
      pdata[i].pos.x = xhigh->x - DELTA;
    }
    if(pdata[i].pos.x < xlow->x+DELTA){
      pdata[i].vel.x = 0.0;
      pdata[i].pos.x = xlow->x + DELTA;
    }
    if(pdata[i].pos.y > xhigh->y-DELTA){
      pdata[i].vel.y = 0.0;
      pdata[i].pos.y = xhigh->y - DELTA;
    }
    if(pdata[i].pos.y < xlow->y + DELTA){
      pdata[i].vel.y = 0.0;
      pdata[i].pos.y = xlow->y + DELTA;
    }
    if(pdata[i].pos.z < xlow->z + DELTA){
      pdata[i].vel.z = 0.0;
      pdata[i].pos.z = xlow->z + DELTA;
    } */
   /* if(pdata[i].pos.z > xhigh->z-DELTA){
      pdata[i].vel.z = 0.0;
      pdata[i].pos.z = xhigh->z - DELTA;
    }*/
  }

}

void periodic_bc(particleData *pdata, int dir)
{
  int i;
#ifdef _OPENMP
#pragma omp parallel for default(shared) private(i) 
#endif
  for(i=0;i<N;i++)
  {
    if((&pdata[i].pos.x)[dir] > (&xhigh->x)[dir]){
      (&pdata[i].pos.x)[dir] = (&pdata[i].pos.x)[dir] 
        - ((&xhigh->x)[dir] - (&xlow->x)[dir]);
    } else if((&pdata[i].pos.x)[dir] < (&xlow->x)[dir]){
      (&pdata[i].pos.x)[dir] = (&pdata[i].pos.x)[dir] 
        + ((&xhigh->x)[dir] - (&xlow->x)[dir]);
    }
  }
  return;
}



void set_interior(particleData *pdata)
{
  int i,count = 0;
//#ifdef _OPENMP
//#pragma omp parallel for default(shared) private(i) 
//#endif
  for(i=0;i<N;i++)
  {
    if(pdata[i].num_neighbors <1 || pdata[i].cellid<0){
      pdata[i].incompressible = 0;
      pdata[i].pressure = 0.0;
      pdata[i].isph_id = -1;
    }else {
      pdata[i].incompressible = 1;
//#pragma omp atomic
      pdata[i].isph_id = count++;
    }
  }
  N_isph = count;
  printf("Particles going to solver: %d\n", N_isph);
}
                                                                                    /* Ghost boundary*/
void wall_proximity(particleData *pdata)
{
  int i;
  for(i=0;i<N;i++){
    if(pdata[i].d1 != NULL){
      free(pdata[i].d1);
      pdata[i].d1 = NULL;
    }
    if(pdata[i].d2 != NULL){
      free(pdata[i].d2);
      pdata[i].d2 = NULL;
    }
    if(pdata[i].d3 != NULL){
      free(pdata[i].d3);
      pdata[i].d3 = NULL;
    }
    if(pdata[i].pos.x<(xhigh->x) && pdata[i].pos.x>(xlow->x) 
        && pdata[i].pos.y>(xlow->y) && pdata[i].pos.y<(xhigh->y)){
      if( fabs(pdata[i].pos.x - xlow->x)/pdata[i].h <= r_cutoff && domain_bc[0]!=0){
        pdata[i].d1 = (vector *)malloc(sizeof(vector));
        pdata[i].d1->x = xlow->x - pdata[i].pos.x;
        pdata[i].d1->y = 0.0; 
        pdata[i].d1->z = 0.0; 
      }else if( fabs(pdata[i].pos.x - xhigh->x)/pdata[i].h <= r_cutoff && domain_bc[1]!=0){
        pdata[i].d1 = (vector *)malloc(sizeof(vector));
        pdata[i].d1->x = xhigh->x - pdata[i].pos.x;
        pdata[i].d1->y = 0.0; 
        pdata[i].d1->z = 0.0; 
      }

      if( fabs(pdata[i].pos.y - xlow->y)/pdata[i].h <= r_cutoff && domain_bc[2]!=0){
        pdata[i].d2 = (vector *)malloc(sizeof(vector));
        pdata[i].d2->y = xlow->y - pdata[i].pos.y;
        pdata[i].d2->x = 0.0; 
        pdata[i].d2->z = 0.0; 
      }else if( fabs(pdata[i].pos.y - xhigh->y)/pdata[i].h <= r_cutoff && domain_bc[3]!=0){
        pdata[i].d2 = (vector *)malloc(sizeof(vector));
        pdata[i].d2->y = xhigh->y - pdata[i].pos.y;
        pdata[i].d2->x = 0.0; 
        pdata[i].d2->z = 0.0; 
      }


    }
    if(dim==3){
      if( pdata[i].pos.z>(xlow->z) && pdata[i].pos.z<(xhigh->z)){
        if( fabs(pdata[i].pos.z - xlow->z)/pdata[i].h <= r_cutoff && domain_bc[4]!=0){
          pdata[i].d3 = (vector *)malloc(sizeof(vector));
          pdata[i].d3->z = xlow->z - pdata[i].pos.z;
          pdata[i].d3->x = 0.0; 
          pdata[i].d3->y = 0.0; 
        }else if( fabs(pdata[i].pos.z - xhigh->z)/pdata[i].h <= r_cutoff && domain_bc[5]!=0){
          pdata[i].d3 = (vector *)malloc(sizeof(vector));
          pdata[i].d3->z = xhigh->z - pdata[i].pos.z;
          pdata[i].d3->x = 0.0; 
          pdata[i].d3->y = 0.0; 
        }
      }
    }

  }
}                     
/* Function to allocate ghost particles and create them. For time step one, 
* malloc is used and thereafter the memory is reallocated
*/
void create_ghost_particles(particleData * pdata)
{
  int i;
  
  wall_proximity(pdata);
  printf("wallproximity done \n");
  int count =0;
  for(i=0;i<N;i++){
    if(pdata[i].d1 != NULL )
      count++;
    if(pdata[i].d2!=NULL)
      count++;
    if(pdata[i].d3!=NULL)
      count++;
    if(pdata[i].d1 != NULL && pdata[i].d2 != NULL)
      count++;
    if(pdata[i].d2 != NULL && pdata[i].d3 != NULL)
      count++;
    if(pdata[i].d1 != NULL && pdata[i].d3 != NULL)
      count++;
    if(pdata[i].d1 != NULL && pdata[i].d2 != NULL && pdata[i].d3 != NULL)
      count++;
  }
  allocate_ghost_particles(count);
  printf("allocated gp \n");
  g_N=count;
  count=0;
  int c;
  for(i=0;i<N;i++){
    if(pdata[i].d1 != NULL){
      for(c=0;c<dim;c++){
        (&g_pdata[count].pos.x)[c] =
          2.0*(&pdata[i].d1->x)[c] + (&pdata[i].pos.x)[c];
      }
      g_pdata[count].real_id = i;
      count++ ;
    }
    if(pdata[i].d2 != NULL){
      for(c=0;c<dim;c++){
        (&g_pdata[count].pos.x)[c] = 2.0*(&pdata[i].d2->x)[c]
          + (&pdata[i].pos.x)[c];
      }
      g_pdata[count].real_id = i;
      count++ ;
    }
    if(pdata[i].d3 != NULL){
      for(c=0;c<dim;c++){
        (&g_pdata[count].pos.x)[c] = 2.0*(&pdata[i].d3->x)[c]
          + (&pdata[i].pos.x)[c];
      }
      g_pdata[count].real_id = i;
      count++ ;
    }
    if(pdata[i].d1 != NULL && pdata[i].d2 != NULL){
      for(c=0;c<dim;c++){
        (&g_pdata[count].pos.x)[c] = 2.0*((&pdata[i].d1->x)[c] + (&pdata[i].d2->x)[c])
          + (&pdata[i].pos.x)[c];
      }
      g_pdata[count].real_id = i;
      count++ ;
    }
    if(pdata[i].d2 != NULL && pdata[i].d3 != NULL){
      for(c=0;c<dim;c++){
        (&g_pdata[count].pos.x)[c] = 2.0*((&pdata[i].d2->x)[c] + (&pdata[i].d3->x)[c])
          + (&pdata[i].pos.x)[c];
      }
      g_pdata[count].real_id = i;
      count++ ;
    }
    if(pdata[i].d1 != NULL && pdata[i].d3 != NULL){
      for(c=0;c<dim;c++){
        (&g_pdata[count].pos.x)[c] = 2.0*((&pdata[i].d1->x)[c] + (&pdata[i].d3->x)[c])
          + (&pdata[i].pos.x)[c];
      }
      g_pdata[count].real_id = i;
      count++ ;
    }
    if(pdata[i].d1 != NULL && pdata[i].d2 != NULL && pdata[i].d3 != NULL ){
      for(c=0;c<dim;c++){
        (&g_pdata[count].pos.x)[c] = 2.0*((&pdata[i].d1->x)[c] 
            + (&pdata[i].d2->x)[c] +(&pdata[i].d3->x)[c]) 
          + (&pdata[i].pos.x)[c];
      }
      g_pdata[count].real_id = i;
      count++ ;
    }
  }
  printf("created gp particles \n");
  update_ghost_velocity(pdata,0);
  printf("vel update \n");
}

static void allocate_ghost_particles(int g_N)
{
  if(sim_time == 0.0 || !(g_pdata)){
    printf("GHOST\n");
    g_pdata = malloc(g_N*sizeof(particleData));
  }else{
    g_pdata = realloc(g_pdata, g_N*sizeof(particleData));

  }
  int i,c;
  /*Initialization*/
  for(i = 0; i<g_N; i++){
    for(c=0;c<3;c++){
      (&g_pdata[i].pos.x)[c] = 0.0;
      (&g_pdata[i].vel.x)[c] = 0.0;

    }
  }
}
void update_ghost_velocity(particleData *pdata,int tag)
{
  int c;
  vector domain_range, midpoint;
  int i,id;
  for(c=0;c<dim;c++){
    (&domain_range.x)[c] = (&xhigh->x)[c] - (&xlow->x)[c]; 
    (&midpoint.x)[c] = (&xlow->x)[c] + 0.5*(&domain_range.x)[c]; 
  }
  int locx,locy,locz;
  vector V_wall;
  for(i=0;i<g_N;i++){
    id = g_pdata[i].real_id;
    locx = (pdata[id].pos.x < midpoint.x)? 0:1; 
    locy = (pdata[id].pos.y < midpoint.y)? 2:3; 
    locz = (pdata[id].pos.z < midpoint.z)? 4:5; 

    if(tag ==1){
      if(dim==2){
        g_pdata[i].vel.x = -pdata[id].vel.x;
        g_pdata[i].vel.y = -pdata[id].vel.y;
        g_pdata[i].vel.z = 0.0;
        if((g_pdata[i].pos.x>xhigh->x || g_pdata[i].pos.x < xlow->x)
            &&(g_pdata[i].pos.y<xhigh->y && g_pdata[i].pos.y>xlow->y)){
          g_pdata[i].vel.y = pdata[id].vel.y;
        }
        if((g_pdata[i].pos.y>xhigh->y || g_pdata[i].pos.y < xlow->y)
            &&(g_pdata[i].pos.x<xhigh->x && g_pdata[i].pos.x>xlow->x)){
          g_pdata[i].vel.x = pdata[id].vel.x;
        }

      }else if(dim ==3){
        g_pdata[i].vel.x = -pdata[id].vel.x;
        g_pdata[i].vel.y = -pdata[id].vel.y;
        g_pdata[i].vel.z = -pdata[id].vel.z;
        if(((g_pdata[i].pos.x>xhigh->x || g_pdata[i].pos.x < xlow->x)
              ||(g_pdata[i].pos.z>xhigh->z || g_pdata[i].pos.z < xlow->z))
            &&(g_pdata[i].pos.y<xhigh->y && g_pdata[i].pos.y>xlow->y)){
          g_pdata[i].vel.y = pdata[id].vel.y;
        }
        if(((g_pdata[i].pos.x>xhigh->x || g_pdata[i].pos.x < xlow->x)
              ||(g_pdata[i].pos.y>xhigh->y || g_pdata[i].pos.y < xlow->y))
            &&(g_pdata[i].pos.z<xhigh->z && g_pdata[i].pos.z>xlow->z)){
          g_pdata[i].vel.z = pdata[id].vel.z;
        }
        if(((g_pdata[i].pos.y>xhigh->y || g_pdata[i].pos.y < xlow->y)
              ||(g_pdata[i].pos.z>xhigh->z || g_pdata[i].pos.z < xlow->z))
            &&(g_pdata[i].pos.x<xhigh->x && g_pdata[i].pos.x>xlow->x)){
          g_pdata[i].vel.x = pdata[id].vel.x;
        }
      }
    }else if(tag == 0){
      if(dim==2){
        if((g_pdata[i].pos.x<xlow->x || g_pdata[i].pos.x>xhigh->x)
            && (g_pdata[i].pos.y<xlow->y || g_pdata[i].pos.y>xhigh->y)){ // corners
          for(c=0;c<dim;c++) 
            (&g_pdata[i].vel.x)[c] = (&pdata[id].vel.x)[c]; 
        }else{
          if(g_pdata[i].pos.x>xhigh->x || g_pdata[i].pos.x < xlow->x){ //right/left
            if(domain_bc[locx] == -2){
              V_wall.x = wall_velocity.x ; V_wall.y = wall_velocity.y;
            }else if(domain_bc[locx] == -1){
              V_wall.x = 0.0; V_wall.y = 0.0;
            }else if(domain_bc[locx] == 1){
              V_wall.x = 0.0; V_wall.y = pdata[id].vel.y;
            }
          }else if(g_pdata[i].pos.y>xhigh->y || g_pdata[i].pos.y < xlow->y ){//top/bottom
            if(domain_bc[locy] == -2){
              V_wall.x = wall_velocity.x ; V_wall.y = wall_velocity.y;
            }else if(domain_bc[locy] == -1){
              V_wall.x = 0.0; V_wall.y = 0.0;
            }else if(domain_bc[locy] == 1){
              V_wall.x = pdata[id].vel.x ; V_wall.y = 0.0;
            }
          }
          for(c=0;c<dim;c++) // no slip 
            (&g_pdata[i].vel.x)[c] = 2.0*(&V_wall.x)[c] - (&pdata[id].vel.x)[c]; 
        }
      }else if(dim ==3){

        if(g_pdata[i].pos.x>xhigh->x || g_pdata[i].pos.x < xlow->x){ //right/left
          if(domain_bc[locx] == -2){
            V_wall.x = wall_velocity.x ; V_wall.y = wall_velocity.y; V_wall.z = wall_velocity.z;
          }else if(domain_bc[locx] == -1){
            V_wall.x = 0.0; V_wall.y = 0.0;V_wall.z = 0.0;

          }else if(domain_bc[locx] == 1){
            V_wall.x = 0.0; V_wall.y = pdata[id].vel.y; V_wall.z = pdata[id].vel.z;
          }
        }else if(g_pdata[i].pos.y>xhigh->y || g_pdata[i].pos.y < xlow->y ){//top/bottom
          if(domain_bc[locy] == -2){
            V_wall.x = wall_velocity.x ; V_wall.y = wall_velocity.y; V_wall.z = wall_velocity.z;
          }else if(domain_bc[locy] == -1){
            V_wall.x = 0.0; V_wall.y = 0.0; V_wall.z = 0.0;
          }else if(domain_bc[locy] == 1){
            V_wall.x = pdata[id].vel.x ; V_wall.y = 0.0;V_wall.z = pdata[id].vel.z ;
          }
        }else if(g_pdata[i].pos.z>xhigh->z || g_pdata[i].pos.z < xlow->z ){//front back
          if(domain_bc[locz] == -2){
            V_wall.x = wall_velocity.x ; V_wall.y = wall_velocity.y; V_wall.z = wall_velocity.z;
          }else if(domain_bc[locz] == -1){
            V_wall.x = 0.0; V_wall.y = 0.0; V_wall.z = 0.0;
          }else if(domain_bc[locz] == 1){
            V_wall.x = pdata[id].vel.x ; V_wall.y = pdata[id].vel.y ;V_wall.z = 0.0;
          }
        }
        
           for(c=0;c<dim;c++) // no slip 
           (&g_pdata[i].vel.x)[c] = 2.0*(&V_wall.x)[c] - (&pdata[id].vel.x)[c]; 

           if((g_pdata[i].pos.x<xlow->x || g_pdata[i].pos.x>xhigh->x)
           && (g_pdata[i].pos.y<xlow->y || g_pdata[i].pos.y>xhigh->y)){ // corners
           g_pdata[i].vel.x = pdata[id].vel.x; 
           g_pdata[i].vel.y = pdata[id].vel.y; 
           //printf("happened x y \n");
           }
           if((g_pdata[i].pos.x<xlow->x || g_pdata[i].pos.x>xhigh->x)
           && (g_pdata[i].pos.z<xlow->z || g_pdata[i].pos.z>xhigh->z)){ // corners
           g_pdata[i].vel.x = pdata[id].vel.x; 
           g_pdata[i].vel.z = pdata[id].vel.z;

           }
           
           if((g_pdata[i].pos.z<xlow->z || g_pdata[i].pos.z>xhigh->z)
           && (g_pdata[i].pos.y<xlow->y || g_pdata[i].pos.y>xhigh->y)){ // corners
           g_pdata[i].vel.z = pdata[id].vel.z; 
           g_pdata[i].vel.y = pdata[id].vel.y; 
           //printf("happened z y \n");
           }
           
      }

    }
  }
}
