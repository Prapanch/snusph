
#include <stdlib.h>
#include <math.h>
#include "kernel.h"
#include "forces.h"

static void viscous_forces_Morris( particleData * p_a, particleData * p_b, interactParams params, boolean ghost);
static void viscous_forces_cleary_monaghan( particleData * p_a, particleData * p_b, interactParams params, boolean ghost);
static void viscous_forces_integral_form( particleData * p_a, particleData * p_b, interactParams params, boolean ghost);
static void viscous_forces_stress( particleData * p_a, particleData * p_b, interactParams params, boolean ghost);
static void viscous_forces_cleary( particleData * p_a, particleData * p_b, interactParams params, boolean ghost);
//static double C_kernel(double r, double h);

/*Compute physical viscous force*/
/*Morris, Fox & Zhu, JCP 1997*/
static void viscous_forces_Morris( particleData * p_a, particleData * p_b, interactParams params, boolean ghost)
{ 
  int i;
  double rdotdwdx = 0.0;
  vector vr;
  for(i=0;i<dim;i++){
    rdotdwdx += (&params.xr.x)[i]*(&params.dwdx.x)[i];
    (&vr.x)[i] = (&p_a->vel.x)[i] - (&p_b->vel.x)[i];
  }

  double Fab = rdotdwdx/(params.rdotr + 0.0001*params.h*params.h);
  double acc;
  double w = params.w;

  for(i = 0; i < dim; i++){
    acc = (p_a->eta+p_b->eta)/(p_a->rho*p_b->rho)*Fab*(&vr.x)[i];
    (&p_a->acc.x)[i] +=  acc*p_b->mass;
    //  (&p_a->vel_xsph.x)[i] +=  (&p_b->vel.x)[i] *w * p_b->mass/p_b->rho;
  //  (&p_b->acc.x)[i] += -acc*p_a->mass;
    //      if(a!=b)
    //    (&p_b->vel_xsph.x)[i] +=  (&p_a->vel.x)[i]* w * p_a->mass/p_a->rho;

  }
}

/*J. Comput. Phys. 148 (1999) 227–264.*/
static void viscous_forces_cleary_monaghan( particleData * p_a, particleData * p_b, interactParams params, boolean ghost)
{ 

  int i;
  double rdotdwdx = 0.0, vdotr =0.0;
  for(i=0;i<dim;i++){
    (&params.vr.x)[i] = (&p_a->vel.x)[i] - (&p_b->vel.x)[i];
    rdotdwdx += (&params.xr.x)[i]*(&params.dwdx.x)[i];
    vdotr += (&params.vr.x)[i]*(&params.xr.x)[i]; 
  }
  double Fab = vdotr/(params.rdotr + 0.0001*params.h*params.h);
  double acc;

  for(i = 0; i < dim; i++){
    if(ghost == false){
    acc = 8.*(((p_a->eta/p_a->rho)+(p_b->eta/p_b->rho))/(p_a->rho+p_b->rho))*Fab*(&params.dwdx.x)[i];   
    (&p_a->acc.x)[i] +=  acc*p_b->mass;
 //   (&p_b->acc.x)[i] += -acc*p_a->mass;
    } else {
    acc = 8.*(((p_a->eta/p_a->rho)+(p_b->eta/p_b->rho))/(p_a->rho+p_b->rho))*Fab*(&params.dwdx.x)[i];   
    (&p_a->acc.x)[i] +=  acc*p_b->mass;

    }
  }
}

/*Integral form of writing second order differentials. Similar to the one used in Cleary 1996 
   CSIRO tech. report for temperature*/
static void viscous_forces_integral_form( particleData * p_a, particleData * p_b, interactParams params, boolean ghost)
{ 
  int i;
  double rdotdwdx = 0.0;
  for(i=0;i<dim;i++){
    (&params.vr.x)[i] = (&p_a->vel.x)[i] - (&p_b->vel.x)[i];
    rdotdwdx += (&params.xr.x)[i]*(&params.dwdx.x)[i];
  }
  double Fab = rdotdwdx/(params.rdotr + 0.0001*params.h*params.h);
  double acc;
  for(i = 0; i < dim; i++){
    acc = 4.*p_a->eta*p_b->eta/(p_a->eta+p_b->eta)/(p_a->rho*p_b->rho)*Fab*(&params.vr.x)[i];       

    (&p_a->acc.x)[i] +=  acc*p_b->mass;
   // (&p_b->acc.x)[i] += -acc*p_a->mass;
  }
}

static void viscous_forces_stress( particleData * p_a, particleData * p_b, interactParams params, boolean ghost){


}
static void viscous_forces_cleary( particleData * p_a, particleData * p_b, interactParams params, boolean ghost){


}

void viscous_forces(particleData * p_a, particleData * p_b, interactParams params,boolean ghost)
{ 
  if(viscous_model < 0 || viscous_model > 4){
    printf("Model not incorporated. Using defaul model: Morris'97");
    viscous_model = 1;
  }
    if(viscous_model==0)
      /*Divergence of stress*/
      viscous_forces_stress(p_a, p_b,params,ghost);
    else if(viscous_model == 1)
      /*Morris97: Works well for low Re flows*/
      viscous_forces_Morris(p_a, p_b,params,ghost);
    else if(viscous_model == 2)
      /*Cleary 1996: Doesn't work that well for low Re flows*/
      viscous_forces_cleary(p_a, p_b,params,ghost);
    else if(viscous_model == 3)
      /*Integral form of second order differentials*/
      viscous_forces_integral_form(p_a, p_b,params,ghost);/*edited prapanj*/
    else if(viscous_model == 4)
      /*Integral form of second order differentials*/
      viscous_forces_cleary_monaghan(p_a, p_b,params,ghost);
    
}

void potential_forces(particleData * p_a, particleData * p_b, interactParams params, boolean ghost)
{
  if(p_a != p_b){
    double sij;
  /*  if(p_a->phase <= 0  &&  p_b->phase <=0)
      sij = 0.0;
    else */if(p_a->phase == p_b->phase)
      sij = potential_params[0];     
    else
//      sij = 0.2*potential_params[1];     
      sij = potential_params[1];     

    vector F;
    int c;
    double local_h = r_cutoff * params.h;
    double w = kernel(params.q,params.h);
    for(c=0;c<dim;c++){
      if(p_a->phase!=p_b->phase){
        if(sqrt(params.rdotr) < 0.3333*local_h)
          (&F.x)[c] =/*10.0*/ 1.0*(cos(1.5*PI*sqrt(params.rdotr)/(local_h)) )* (&params.xr.x)[c]/sqrt(params.rdotr) ;
        else
          (&F.x)[c] = (cos(1.5*PI*sqrt(params.rdotr)/(local_h)) )* (&params.xr.x)[c]/sqrt(params.rdotr) ;

      }else{
        (&F.x)[c] = (cos(1.5*PI*sqrt(params.rdotr)/(local_h)) )* (&params.xr.x)[c]/sqrt(params.rdotr) ;
      }
      if(params.rdotr == 0.0){
        printf( " Zero in rdotr \n)") ; exit(1);}
    // if(p_a->phase>=0)
      (&p_a->acc.x)[c] +=  sij*(&F.x)[c]/p_a->mass;
    // if(p_b->phase>=0)
     //   (&p_b->acc.x)[c] +=  - sij* (&F.x)[c]/p_b->mass; 
    }
  }
}
/*double C_kernel(double r, double h)
{
  double C;
  C= (32.0/PI*pow(h,9));


}*/
