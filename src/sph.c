/** @mainpage SNU-SPH Solver Documentation 
 *
 * <tt>
\verbatim
                            ___ ___ ___ _  _   ___ _                
    .---. .-. .-..-. .-.   |_ _/ __| _ \ || | | __| |_____ __ __     
   ( .-._)|  \| || | | |    | |\__ \  _/ __ | | _|| / _ \ V  V /     
  (_) \   |   | || | | |   |___|___/_| |_||_| |_| |_\___/\_/\_/      
  _  \ \  | |\  || | | |   / __| ___| |_ _____ _ _                   
 ( `-'  ) | | |)|| `-')|   \__ \/ _ \ \ V / -_) '_|                   
  `----'  /(  (_)`---(_)   |___/\___/_|\_/\___|_|                     
         (__)                                                        
\endverbatim 
</tt>
 *  @section intro Introduction
 *  This is an Incompressible SPH Sover for the simulation of flows with the 
 *  following challenges:
 *  
 *  1. Multiphase flows
 *  2. Surface tension
 *  3. Contact line dynamics
 *  4. Complex geometries
 *  5. Rigid Body interactions
 *
 *  The code used OpenMP for parallelization. 
 */
/**
 * @file sph.c
 * @author Prapanch Nair
 * @author Gaurav Tomar
 * @date @f \today @f
 * @brief This is the file containing the main function.
 * 
 * The file has functions for initializing libraries, initiating the main data
 * and deleting pointers at end of execution
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <memory.h>
#ifdef _OPENMP
    #include <omp.h>
#endif
#include "lis.h"
#include "sph.h"
#include "particles.h"
#include "integrator.h"
#include "kernel.h"
#include "write.h"
#include "run.h"
#include "read_input.h"

/**
 * @brief The main function
 *
 * @param argc No. of command line inputs
 * @params argv[] Command line input like the input file name etc.
 *
 * @return nothing
 */
int main(int argc, char *argv[])
{
  print_logo();
#ifdef _OPENMP
  start_wall_time =  omp_get_wtime();
#endif
  int index = 2;
  if (index >= argc){ /* missing FILE */
    printf ("Error: Insufficient arguments\n");
     printf ("./SPH 0(for initializing from 0 time) input_file \n Or \n");       
         printf ("./SPH 1(for restarting from file) restart_file \n ");
    exit(1);
  }
  particleData *pdata = NULL;	
  restart = atoi(argv[1]);
  FILE *fptr = fopen (argv[index], "r");
  if (fptr == NULL) {
    printf ("Unable to open input/restart file %s\n Execution format: SPH 0/1 file\n", argv[index]);
    exit(1);
}
  if(restart){
    pdata = read_restart(fptr);
    fclose(fptr);
  }else  {
    pdata = read_input(fptr);
    fclose(fptr);
  }
  property_assign(pdata);
  initialize_kernel_coefficients(pdata[0].h);
#ifdef _OPENMP
  printf("OPENMP: processors %d, max threads %d \n",omp_get_num_procs(), 
      omp_get_max_threads());
#endif
  /* Linear Iterative Solver Library*/
  lis_initialize(&argc, &argv);
  run(pdata,restart);    
  lis_finalize();

  free(pdata); free(xlow); free(xhigh);
  free(cells.partincell); free(cells.cellofpart); free(cells.pos);
  free(cells.coord);

#ifdef _OPENMP
  printf("Time taken for the simulation: %lf",omp_get_wtime()-start_wall_time); 
#endif
  return 0;
}
// \033[1;34m
void print_logo()
{
printf("                           ___ ___ ___ _  _   ___ _             \n");   
printf("   .---. .-. .-..-. .-.   |_ _/ __| _ \\ || | | __| |_____ __ __ \n");    
printf("  ( .-._)|  \\| || | | |    | |\\__ \\  _/ __ | | _|| / _ \\ V  V /\n");    
printf(" \033[0;34m(_)\033[0m \\   |   | || | | |   |___|___/_| |_||_| |_| |_\\___/\\_/\\_/ \n");    
printf(" _  \\ \\  | |\\  || | | |   / __| ___| |_ _____ _ _               \n");  
printf("( `-'  ) | | |\033[0;34m)\033[0m|| `-'\033[0;34m)\033[0m|   \\__ \\/ _ \\ \\ V / -_) '_|             \n");  
printf(" `----' \033[0;34m /(  (_)\033[0m`---\033[0;34m(_)\033[0m   |___/\\___/_|\\_/\\___|_|                \n");  
printf("\033[0;34m        (__)                                                    \n\033[0m");    
return;
}
