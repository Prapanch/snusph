#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "kernel.h"

static double kernel_M2spline(double q, double h);
static double kernel_M4spline(double q, double h);
static double kernel_Gaussian(double q, double h);
static double kernel_spline5(double q,double h);
static double kernel_quartic(double q, double h);
static double kernel_wendland(double q, double h);

static double fab_M2spline(double q, double h);
static double fab_M4spline(double q, double h);
static double fab_Gaussian(double q, double h);
static double fab_spline5(double q,double h);
static double fab_quartic(double q,double h);
static double fab_wendland(double q,double h);

void initialize_kernel_coefficients(double h)
{
  switch(kernel_model){
    case 0:
      /*M2 Spline*/
      break;
    case 1:
      /*M4 Spline*/
      coeff[0] = 1.;
      coeff[1] = 15./(7.*PI);
      coeff[2] = 3./(2.*PI);
      break;
    case 2:
      /*Gaussian Kernel*/
      break;
    case 3:
      /*Fifth order spline with 3h support*/
      coeff[0] = 120.;
      coeff[1] = 7./(478.*PI);
      coeff[2] = 3./(359.*PI);
      break;
    case 4:
      /*Quartic kernel with 2.5h support*/
      coeff[0] = 120.;/*unkown for 1D*/
      coeff[1] = 0.0255;
      coeff[2] = 3./(359.*PI);/*unknown for 3D*/
      break;
    case 5:
      /*Quintic Wendland kernel with 2.0h support*/
      coeff[0] = 120.;/*unkown for 1D*/
      coeff[1] = 7./(4.*PI);
      coeff[2] = 21./(16.*PI);
      alpha_fab = -5.0 * coeff[dim-1]/pow(h,dim+2) ; /* used to reduce FLOP*/
      break;
  }
}

double kernel(double q, double h)
{
  double w = 0.;

  switch(kernel_model){
  case 0:
  /*M2 Spline*/
    w = kernel_M2spline(q, h);
    break;
  case 1:
  /*M4 Spline*/
    w = kernel_M4spline(q, h);
    break;
  case 2:
  /*Gaussian Kernel*/
    w = kernel_Gaussian(q, h);
    break;
  case 3:
    /*Fifth order spline with 3h support*/
    w = kernel_spline5(q,h);    
    break;
  case 4:
    /*Quartic kernel with 2.5h support*/
    w = kernel_quartic(q,h);    
    break;
  case 5:
    /*Quintic Wendland kernel with 2.0h support*/
    w = kernel_wendland(q,h);    
    break;
  }

  return w;
}

double kernel_Fab(double q, double h)
{

  
  double fx = 0.0;
   
  switch(kernel_model){
  case 0:
  /*M2 Spline*/
    fx = fab_M2spline(q, h);
    break;
  case 1:
  /*M4 Spline*/
    fx = fab_M4spline(q, h);
    break;
  case 2:
  /*Gaussian Kernel*/
    fx = fab_Gaussian(q, h);
    break;
  case 3:
    /*Fifth order spline with 3h support*/
    fx = fab_spline5(q,h);
    break;
  case 4:
    /*Quartic kernel with 2.5h support- Cummins and Rudman*/
    fx = fab_quartic(q,h);
    break;
  case 5:
    /*Quintic Wendland kernel with 2.0h support*/
    fx = fab_wendland(q,h);
    break;
  }
  
  return fx;
}

static double kernel_M2spline(double q, double h)
{

  double w;
  if(q>=0 && q<=1)
    w = (1-q);
  else
    w = 0.;

  return w;
}

static double kernel_M4spline(double q, double h)
{
  double w = 0;
  if(q>=0 && q<=1)
    w = coeff[dim-1]*(2./3. - q*q + q*q*q/2.)/pow(h,dim);     
  else if(q>=1 && q<=2)
    w = coeff[dim-1]*pow(2-q,3)/(6.*pow(h,dim));
  
  return w;
}

static double kernel_Gaussian(double q, double h)
{
  double A;
  double w;
  if(dim==1) A = 1.04823;
  else if (dim==2) A = 1.10081;
  else A = 1.18516;
  if(q*(q-2)<=0.){
    w = A/(pow(h*sqrt(PI),dim))*(exp(-q*q)-exp(-4));
    // printf("q,h&w: %lf %lf %lf\n",q,h,w);
    return w;
  }
  else
    return 0.;
}

static double kernel_spline5(double q, double h)
{
  double w = 0;
  if(q>=0 && q<=1)
    w = coeff[dim-1]/pow(h,dim)*(pow(3-q,5)-6.*pow(2-q,5) + 15.*pow(1-q,5));     
  else if(q>=1 && q<=2)
    w = coeff[dim-1]/pow(h,dim)*(pow(3-q,5)-6.*pow(2-q,5));
  else if(q>2 && q <=3)
    w = coeff[dim-1]/pow(h,dim)*(pow(3-q,5));
  
  return w;
}
static double kernel_quartic(double q, double h)
{
  double w = 0;
  if(q>=0 && q<0.5)
    w = 0.0255*(pow(2.5-q,4)-5.*pow(1.5-q,4) + 10.*pow(0.5-q,4))/pow(h,dim);     
  else if(q>=0.5 && q<1.5)
    w = 0.0255*(pow(2.5-q,4)-5.*pow(1.5-q,4))/pow(h,dim);
  else if(q>=1.5 && q <2.5)
    w = 0.0255*(pow(2.5-q,4))/pow(h,dim);
  
  return w;
}
static double kernel_wendland(double q, double h)
{
  double w = 0;
  if(q>=0 && q<=2)
    w = (coeff[dim-1]/pow(h,dim))*(pow(1-(0.5*q),4)*(2.0*q +1));     
  
  return w;
}

/*!!M2spline first derivative is discontinuous!!*/
static double fab_M2spline(double q, double h)
{

  double w;
  if(q>=0 && q<=1)
    w = -1./(q*pow(h,dim+2));
  else
    w = 0.;
  return w;
}

static double fab_M4spline(double q, double h)
{
  double w = 0.;
  if(q*(q-1) <= 0.)
    w = coeff[dim-1]*(-2. + 1.5*q)/(pow(h,dim+2));
  else if((q-1)*(q-2)<= 0.)
    w = coeff[dim-1]*(2. - q)*(q - 2.)/(2.*q*pow(h,dim+2));

  return w;
}

static double fab_Gaussian(double q, double h)
{
    double A;
    if(dim==1) A = 1.04823;
    else if (dim==2) A = 1.10081;
    else A = 1.18516;

    if(q>=0 && q<=2)
      {
	double w = -2*A/(pow(sqrt(PI),dim)*pow(h,dim+2))*exp(-q*q);
	return w;
      }
    else
      return 0;	
}

static double fab_spline5(double q, double h)
{
  double w = 0.;
  if(q==0)
    w = -120.*coeff[dim-1]/(pow(h,dim+2));
  else if(q>0 && q<=1)
    w = coeff[dim-1]/(q*pow(h,dim+2))*(-5.*pow(3-q,4)+30.*pow(2-q,4) - 75.*pow(1-q,4));     
  else if(q>=1 && q<=2)
    w = coeff[dim-1]/(q*pow(h,dim+2))*(-5*pow(3-q,4)+30.*pow(2-q,4));
  else if(q>2 && q <=3)
    w = coeff[dim-1]/(q*pow(h,dim+2))*(-5*pow(3-q,4));
  return w;

}

static double fab_quartic(double q, double h)
{
  double w = 0.;
  if(q>=0 && q<0.5)
    w = 0.0255*4.*(-pow(2.5-q,3)+5.*pow(1.5-q,3) - 10.*pow(0.5-q,3))/(q*pow(h,dim+2));
  else if(q>=0.5 && q<1.5)
    w = 0.0255*4.*(-pow(2.5-q,3)+5.*pow(1.5-q,3))/(q*pow(h,dim+2));
  else if(q>=1.5 && q <2.5)
    w = 0.0255*4.*(-pow(2.5-q,3))/(q*pow(h,dim+2));
  return w;
}

static double fab_wendland(double q, double h)
{
  double w = 0.;
  if(q==0)
    w = -120.*coeff[dim-1]/(pow(h,dim+2));
  else if(q>0 && q<=2.0)
   // w = (coeff[dim-1]/(q*pow(h,dim+2)))*(5.0*q*pow(1.0-0.5*q,3.0));     
    w = alpha_fab*(pow(1.0-0.5*q,3.0));     
  return w;
}




