#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sph.h"
#include "integrator.h"
#include "forces.h"
#include "isph.h"
#include "run.h"
#include "boundary.h"
#include "kernel.h"

static void predictor(particleData *pdata, double dt, int tag);
static void corrector(particleData *pdata, double dt);
//static void interface_id(particleData *pdata);
static void save_prev(particleData *pdata);
//static void calculate_area_ab(particleData *pdata);                            
static void update_divergence(particleData *pdata);
static void sum_divergence(particleData *pdata);

void integrate_isph_cummins(particleData *pdata, double dt)
{
  // calculate_area_ab(pdata)    ;                         
  //  calculate_area_ab(pdata);// for drop

  save_prev(pdata); 

  if(rigid_body_sim)
    cg_rigid(pdata);

  predictor(pdata,dt,1);

//  if(GHOSTBC){
//    wall_proximity(pdata);
//  }
  interact_type itype[1];
  itype[0]=visc;
  interact(pdata,itype, 1 ,dt); /*viscous force*/ 
  //if(sim_time == 0.0)
  //  interface_id(pdata);
  /*
   * find the edge particles
   * do an interaction among edge particles and pick neighbors
   * go through all edge particles and find the centre of oscular circle
   * with that circle get normal and curvature
   * impart surface tension (diffused) force to all particles near edges
   */


  //  interact(pdata,surfNorm,dt);
  //  interact(pdata,interfaceNeighbors,dt);
  if(surf_tens){
    interface_arrange(pdata);
    calculate_threepoint_kappa(pdata);
  }
  //  interact(pdata,surfKappa,dt);
  body_forces(pdata); /*body force*/
  /*  if(surf_tens)
      surface_tension(pdata);
   */
  predictor(pdata, dt, 2); /* update velocity to u* */
  set_interior(pdata); 
  predictor(pdata, dt, 4); 

if(GHOSTBC)
  update_ghost_velocity(pdata,1);

  if(defgrad){
  itype[0]=defGrad;
    interact(pdata,itype, 1,dt);
    compute_determinant_F(pdata, dt);
  }else{
    itype[0]=divV;
    interact(pdata,itype, 1 ,dt); /* divergence of u* */
  }
    sum_divergence(pdata);
if(divergence==4){
    update_divergence(pdata);
  }
  //write(pdata,0);
 //   write(pdata, 5000);                                                         

solve_pressure(pdata,dt); 
itype[0]= pressForce;
  interact(pdata,itype, 1,dt);/*pressure force*/
  predictor(pdata, dt, 3); 
  if(rigid_body_sim)
    rigid_body(pdata,dt);

  corrector(pdata,dt);
  int i;
  for(i=0; i<dim; i++)
    if(periodic[i] == 1) periodic_bc(pdata,i);
boundary_condition(pdata,0);
}

static void save_prev(particleData *pdata)
{
  int i,c,d;
  nnz = 0;
  for(c = 0; c < dim; c++){
    int body_id;
    for(body_id=0;body_id<10;body_id++){
      (&V_r_n[body_id].x)[c] = (&V_r[body_id].x)[c];
      (&omega_n[body_id].x)[c] = (&omega[body_id].x)[c];
      (&c_g[body_id].x)[c] = 0.0;
    }
  }
#ifdef _OPENMP
#pragma omp parallel for default(shared) private(i,c,d) 
#endif
  for(i=0;i<N;i++){
    for(c = 0; c < dim; c++){
      (&pdata[i].posn.x)[c] = (&pdata[i].pos.x)[c];
      (&pdata[i].veln.x)[c] = (&pdata[i].vel.x)[c];  
      (&pdata[i].acc.x)[c] = 0.0;  
      (&pdata[i].acc_p.x)[c] = 0.0;
      (&pdata[i].acc_mu.x)[c] = 0.0;
      (&pdata[i].n.x)[c] = 0.0;  
      (&pdata[i].nearest.x)[c] = 0.0;  
      (&pdata[i].gradW.x)[c] = 0.0;  
      (&pdata[i].uxx.x)[c] = 0.0;  
    }   
    for(c=0;c<dim;c++)
      for(d=0;d<dim;d++)
        (&(&pdata[i].def_grad_n.xx)[c].x)[d] = 0.0;

    pdata[i].num_neighbors = 0;
    pdata[i].wsum = 0.0;
    pdata[i].u = 0.0;
    pdata[i].div_pos = 0.0;
    pdata[i].kab = 0.0;
    pdata[i].pressuren = pdata[i].pressure;
  }
}


static void predictor(particleData *pdata, double dt, int tag)
{
  int i,c;
#ifdef _OPENMP
#pragma omp parallel for default(shared) private(i,c) 
#endif
  for(i=0;i<N;i++){
    if(tag==2){
      for(c=0;c<dim;c++)
        (&pdata[i].acc_mu.x)[c] = (&pdata[i].acc.x)[c];
    }
    if(tag==3){
      for(c=0;c<dim;c++)
        (&pdata[i].acc_p.x)[c] = (&pdata[i].acc.x)[c];
    }

    if(pdata[i].phase >=0){
    for(c = 0; c < dim; c++){
      if(tag==1 ){
        (&pdata[i].pos.x)[c]   = (&pdata[i].posn.x)[c]  + dt*((&pdata[i].veln.x)[c]) ; /*updating position based on old vel*/
      }
    }
    }
    if(pdata[i].phase >=0){
      for(c = 0; c < dim; c++){
        if(tag==2){
          (&pdata[i].vel.x)[c]   = (&pdata[i].veln.x)[c] + dt*((&pdata[i].acc.x)[c]) ;/*velocity based on visc and body forces*/
        }else if(tag==3){
          (&pdata[i].vel.x)[c]   = (&pdata[i].vel.x)[c] + dt*((&pdata[i].acc.x)[c]) ;
        }else if(tag==4){
          (&pdata[i].pos_s.x)[c] =  dt*((&pdata[i].vel.x)[c]) ;
        }
      }
    }
  } 
}
 
static void corrector(particleData *pdata,double dt)
{
  int i,c ;
#ifdef _OPENMP
#pragma omp parallel for default(shared) private(i,c) 
#endif
  for(i=0;i<N;i++){
    if (pdata[i].phase>=0 ) { 
      for(c = 0; c < dim; c++)
        (&pdata[i].pos.x)[c] = (&pdata[i].posn.x)[c] + dt*0.5*((&pdata[i].vel.x)[c] + (&pdata[i].veln.x)[c]) ;
  //      (&pdata[i].pos.x)[c] = (&pdata[i].posn.x)[c] + dt*((&pdata[i].vel.x)[c] ) ;
    } 
  }
}
/*
static void interface_id(particleData *pdata)
{
  int i ;
#ifdef _OPENMP
#pragma omp parallel for default(shared) private(i,c) 
#endif
  for(i=0;i<N;i++){
    if (fabs(pdata[i].div_pos) <= 1.40 ) { 
      pdata[i].interface_tag = 1;
    }else {
      pdata[i].interface_tag = 0;
    } 
  }
}
static void calculate_area_ab(particleData *pdata)                             
{                                                                              

  int i;                                                                       
  double x_max=0.0, y_max=0.0;                                                 
  double x_min=0.0, y_min=0.0;                                                 
  for(i=0;i<N;i++){                                                            

    if(pdata[i].pos.x>x_max)                                                   
      x_max = pdata[i].pos.x;                                                  
    if(pdata[i].pos.y>y_max)                                                   
      y_max = pdata[i].pos.y;                                                  
    if(pdata[i].pos.x<x_min)                                                   
      x_min = pdata[i].pos.x;                                                  
    if(pdata[i].pos.y<y_min)                                                   
      y_min = pdata[i].pos.y;                                                  

  }                                                                            
  major_axis = (x_max-x_min)/2.0;                                   

}
*/
static void update_divergence(particleData *pdata)
{
int i;
	for(i=0;i<N;i++){
		pdata[i].u = pdata[i].uxx.x/pdata[i].gradW.x + pdata[i].uxx.y/pdata[i].gradW.y;

	}
	return ;
}
static void sum_divergence(particleData *pdata)
{
  int i;
  double sum = 0.0;
  for(i=0;i<N;i++){
    sum += pdata[i].u;
  }
   printf("\n Sum of divergence = %lf \n" , sum); 
}
