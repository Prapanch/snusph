#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "kernel.h"
#include "boundary.h"
#include "sph.h"
#include "run.h"
#include "forces.h"

//static void cross_product(vector a, vector b, vector *c,int dim);

void rigid_body(particleData * pdata, double dt)
{
  int i,c,body_id;
  int dim_rigid = 3;
  vector periodic_corr;
  vector angular_acc[10];
  for(c=0;c<dim_rigid;c++)                                                           
    (&periodic_corr.x)[c] = (&xhigh->x)[c] - (&xlow->x)[c];  
  //  vector sum_forces ;
  vector sum_torques[10] ;
  vector surface_normal[10];
  for(body_id=0;body_id<10;body_id++){
    for(c=0;c<dim_rigid;c++){
      (&force_r[body_id].x)[c] = 0.0;
      (&sum_torques[body_id].x)[c] = 0.0;
      (&surface_normal[body_id].x)[c] = 0.0;
    }
  }
  if(rigid_fixed_velocity == false){
    for(i=0;i<N;i++){

      vector tot_acc;
      if(pdata[i].phase == 0 /*&& p->ncount==1*/)
      {
        body_id = pdata[i].body_id;
        for(c=0; c<dim_rigid; c++){
          (&tot_acc.x)[c] = 1.0*(&pdata[i].acc_p.x)[c] + 1.0*(&pdata[i].acc_mu.x)[c];
        }

        for(c=0;c<dim_rigid;c++){
          (&force_r[body_id].x)[c] += pdata[i].mass* (&tot_acc.x)[c];
        }

        /* sum of torques for the angular velocity*/
        vector xr;/* moment arm*/
        xr.x =0.;xr.y=0.;xr.z=0.;

        for(c=0;c<dim_rigid;c++){
          (&xr.x)[c] = (&pdata[i].pos.x)[c] - (&c_g[body_id].x)[c];
          if(periodic[c] && fabs(((&xr.x)[c])>0.5*fabs((&periodic_corr.x)[c]))){
            if((&xr.x)[c] > 0.)
              (&xr.x)[c] += - (&periodic_corr.x)[c];
            else
              (&xr.x)[c] += (&periodic_corr.x)[c];
          }
        }
        vector rcrossf;
        cross_product(xr, tot_acc, &rcrossf, dim);
        for(c=0;c<dim_rigid;c++){
          (&sum_torques[body_id].x)[c] += pdata[i].mass* (&rcrossf.x)[c];
        }
      }

    }
  }

  // we have sum_torques, force.
  // divide by MI and mass, to get angular acc and acc
  // make acc_mu=0 and acc_p = total_acc + tangential acc
  //
  //  rigid_fixed_velocity = false;
  /*update velocity, angular velocity of rigid body*/

  for(body_id=0;body_id<10;body_id++){
    for(c=0;c<dim_rigid;c++){
      if(rigid_fixed_velocity==false){
        (&V_r[body_id].x)[c] = (&V_r_n[body_id].x)[c]+ (dt/m_r[body_id])*(&force_r[body_id].x)[c];
        (&omega[body_id].x)[c] = (&omega_n[body_id].x)[c] + (dt/m_i[body_id])*(&sum_torques[body_id].x)[c];
        (&angular_acc[body_id].x)[c] = (&sum_torques[body_id].x)[c]/m_i[body_id];
      }else{
        (&omega[body_id].x)[c] = 0.;
      }
    }
  }

  for(i=0;i<N;i++){
    if(pdata[i].phase == 0){
      body_id =  pdata[i].body_id;
      vector xr;/* moment arm*/
      xr.x= 0.0 ; xr.y=0.0; xr.z=0.0;
      for(c=0;c<dim_rigid;c++){
        (&xr.x)[c] = (&pdata[i].pos.x)[c] - (&c_g[body_id].x)[c];
      }
      vector omegacrossr;
      vector alphacrossr;
      cross_product(omega[body_id], xr, &omegacrossr, dim);
      cross_product(angular_acc[body_id], xr, &alphacrossr, dim);
      /*      p->vel.x = V_r.x -  omega*xr.y;
              p->vel.y = V_r.y +  omega*xr.x;*/
      for(c=0;c<dim_rigid;c++){
        if(rigid_fixed_velocity == false){
          (&pdata[i].vel.x)[c] = (&V_r[body_id].x)[c] + (&omegacrossr.x)[c];
          (&pdata[i].acc_p.x)[c] = ((&force_r[body_id].x)[c]/m_r[body_id]) + (&alphacrossr.x)[c];
          (&pdata[i].acc_mu.x)[c] = 0.0;
        }else{
          (&pdata[i].vel.x)[c] = (&V_r[body_id].x)[c] ;
          (&pdata[i].acc_p.x)[c] = (&V_r[body_id].x)[c]/dt ;
          (&pdata[i].acc_mu.x)[c] = 0.0;

        }
      }

    }

  }
  printf("\n Omega value %lf %lf %lf", omega[2].x, omega[2].y, omega[2].z);
  //printf("\n Omegacorssr value %lf %lf %lf\n", omegacrossr.x, omegacrossr.y, omegacrossr.z);
  return;
}

void cg_rigid(particleData * pdata)
{
  int i,c,body_id;
  double vol[10];
  for(body_id=0;body_id<10;body_id++){
    m_i[body_id] = 0.0;
    m_r[body_id] = 0.0;
    vol[10]= 0.0;
  }
  vector cg_comp[10], periodic_corr;
  int check = 0;

  for(c=0;c<dim;c++)                                                           
    (&periodic_corr.x)[c] = (&xhigh->x)[c] - (&xlow->x)[c];   

  for(i=0;i<N;i++){
    if(pdata[i].phase == 0){
      body_id =  pdata[i].body_id;

      if(check == 0){
        cg_comp[body_id] = pdata[i].pos;
        check = 1;
      }

      for(c=0;c<dim;c++){

        if((fabs((&cg_comp[body_id].x)[c] - (&pdata[i].pos.x)[c]) > 0.5*(&periodic_corr.x)[c]) && periodic[c]){
          if((&pdata[i].pos.x)[i] < 0.5 * (&periodic_corr.x)[c])
            (&c_g[body_id].x)[c] += (pdata[i].mass/pdata[i].rho)*((&pdata[i].pos.x)[c] + (&periodic_corr.x)[c]);
        }
        else{
          (&c_g[body_id].x)[c] += (pdata[i].mass/pdata[i].rho)*(&pdata[i].pos.x)[c];
        }
      }
      vol[body_id] += pdata[i].mass/pdata[i].rho;
      //printf(" , phase \n",i); 
      if(pdata[i].rho == 0.0){
        printf(" 0 rho %d, phase \n",i); exit(1);
      }
    }
  }

  for(c=0;c<dim;c++)
    (&c_g[body_id].x)[c] = (&c_g[body_id].x)[c]/vol[body_id];

  for(c=0;c<dim;c++){                                                                       
    if((&c_g[body_id].x)[c] > (&xhigh->x)[c]){                             
      (&c_g[body_id].x)[c] -= (&periodic_corr.x)[c];                          
    } else if((&c_g[body_id].x)[c] < (&xlow->x)[c]){                       
      (&c_g[body_id].x)[c] += (&periodic_corr.x)[c];                          
    }                                                                          
  }            

  printf("CG of rigid: %4.3g %4.3g %4.3g \n", c_g[2].x, c_g[2].y, c_g[2].z); 

  for(i=0;i<N;i++){
    if(pdata[i].phase == 0){
      body_id = pdata[i].body_id;
      double rdotr = 0.0;
      for(c=0;c<dim;c++)
        rdotr += ((&pdata[i].pos.x)[c]- (&c_g[body_id].x)[c])
          * ((&pdata[i].pos.x)[c]- (&c_g[body_id].x)[c]); 

      m_i[body_id] += pdata[i].mass*rdotr;
      m_r[body_id] += pdata[i].mass;
    }
  }
  return;
}

/*
static void cross_product(vector a, vector b, vector *c, int dim)
{

  c->x  = a.y*b.z - a.z*b.y;
  c->y  = a.z*b.x - a.x*b.z;
  c->z  = a.x*b.y - a.y*b.x;
  if(dim ==2){
    c->x =0.0;
    c->y = 0.0;
  }
  return;
}
*/


