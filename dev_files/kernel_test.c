#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "sph.h"
#include "particles.h"
#include "kernel.h"
void test_kernel_1D(particles *plist)
{
  particles *p_save = plist;
  /*Kernel shapes*/
  FILE *fp = fopen("kernels.dat", "w");
  double q = 0, h = 0.1, ksum;
  double x, f,fx;
  for(q = 0; q< 3; q+= 0.01){       
    f = kernel(q,h);
    fx = kernel_Fab(q, h);
    fprintf(fp, "%lf %lf %lf\n", q, f, fx);      
  }  
  fclose(fp);

  /*Assign function: (x - 0.5)^4 to the particles*/
  fp = fopen("kernelTest-pfunc1D.dat", "w");
  while(plist!=NULL){
    particle *p = plist->p;
    p->func = pow(p->pos.x - 0.5, 4);
    fprintf(fp, "%lf %lf\n",p->pos.x, p->func);
    plist = plist->next;
  }

  fclose(fp);

  fp = fopen("kernelTest1D.dat", "w");
  /*Write f, f,x and f,xx*/
  for(x = 0; x < 1.; x += 0.01){
    f = 0.;
    fx = 0.;
    ksum = 0.;
    plist = p_save;
    while(plist!=NULL){
      particle *p = plist->p;
      q = fabs(x - p->pos.x)/p->h;
      f += p->func*kernel(q,p->h)*p->mass/p->rho;
      plist = plist->next;
    }

    plist = p_save;
    while(plist!=NULL){
      particle *p = plist->p;
      q = fabs(x - p->pos.x)/p->h;
      fx += (p->func)*kernel_Fab(q,p->h)*(x-p->pos.x)*p->mass/p->rho;
      plist = plist->next;
    }
    
    fprintf(fp, "%lf %lf %lf %lf %lf\n",x, f, fx, pow(x-0.5,4), 4.*pow(x-0.5,3));
  }

  fclose(fp);
}

void test_kernel_2D(particles *plist)
{ 
  particles *p_save = plist;

  FILE *fp;

  /*Kernel shapes*/
  fp = fopen("kernels.dat", "w");
  double q = 0, h = 0.1, ksum;
  double x, f,fx;
  for(q = 0; q< 3; q+= 0.01){       
    f = kernel(q,h);
    fx = kernel_Fab(q, h);
    fprintf(fp, "%lf %lf %lf\n", q, f, fx);      
  }  
  fclose(fp);

  fp = fopen("kernel-pfunc.dat", "w");
  double y,fy;  
  while(plist!=NULL){
    particle *p = plist->p;
    p->func = sin(PI*p->pos.x)*sin(PI*p->pos.y);
    double der_x = PI*cos(PI*p->pos.x)*sin(PI*p->pos.y);
    double der_y = PI*cos(PI*p->pos.y)*sin(PI*p->pos.x);
    fprintf(fp, "%lf %lf %lf %lf %lf\n",p->pos.x, p->pos.y, p->func,der_x,der_y);
    plist = plist->next;	
  }
  
  fclose(fp);
  
  fp = fopen("kernelTest2D.dat", "w");
  for(x = 0; x < 1.; x += 0.01){
    for(y = 0; y< 1.; y += 0.01){
      f = 0.;
      fx = 0.;
      fy = 0.;
      plist = p_save;
      while(plist!=NULL){
	particle *p = plist->p;
	plist = plist->next;
	q = sqrt((x - p->pos.x)*(x - p->pos.x) + (y - p->pos.y)*(y - p->pos.y))/p->h;
	if(q > 2.) continue;
	f += /*p->func*/kernel(q,p->h)*p->mass/p->rho;	   
	fx += (p->func)*kernel_Fab(q,p->h)*(x - p->pos.x)*p->mass/p->rho;
	fy += (p->func)*kernel_Fab(q,p->h)*(y - p->pos.y)*p->mass/p->rho;	    	
      }      
      fprintf(fp, "%lf %lf %lf %lf %lf %lf %lf %lf\n",x, y,f,fx,fy, f - sin(PI*x)*sin(PI*y), fx - PI*cos(PI*x)*sin(PI*y), fy - PI*sin(PI*x)*cos(PI*y));
    }
  }
}
