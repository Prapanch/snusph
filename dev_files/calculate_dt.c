#include<stdio.h>
#include<math.h>
#include<stdlib.h>

#include "forces.h"
#include "run.h"

double calculate_dt(particles *plist)
{
	double u_max, u_ref;
        u_max = 0.0;
        u_ref = 0.0;
/*
	if (press_model==0){
		u_ref= sqrt(press_params[0] * press_params[1]/press_params[2]);
	}
	else if (press_model==1){
		u_ref= press_params[0];
	}*/
	
	double h_0,eta_0;
	while(plist){
		particle *p = plist->p;

                if(p->incompressible ==1)
                  u_ref = sqrt(p->vel.x*p->vel.x + p->vel.y*p->vel.y + p->vel.z*p->vel.z);

                /*if(u_ref >=20.0) u_ref = 0;*/

                if(u_ref >u_max) 
                  u_max = u_ref;
	/*	if(acc_R>max_acc_R)
			max_acc_R = acc_R;
		if(p->id == 1){
			h_0 = p->h;
			eta_0 = p->eta;
		}*/
		plist=plist->next;
	}

	//double first = 0.4* h_0/u_ref;
	//double second = 0.25*sqrt(h_0/max_acc_R);
	//double third = 0.125*pow(h_0,2)/eta_0;
/*	double first = 0.1 * 0.00125/u_max;*/
	double first = 0.1 * particle_dx/u_max;
/*	double second = 0.005*sqrt(h_0/max_acc_R);*/
/*	double third = 0.125*pow(h_0,2)/eta_0;*/
	double second = 0.001;

        printf("\n \t U_max= %lf ", u_max);


        if(first < second)
          return first;
        else 
          return second;
        /*
	if (second<MIN)
		MIN = second;

	if(third<MIN)
		MIN = third;
*/
//	*dt = MIN;



}

