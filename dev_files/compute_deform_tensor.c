#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "sph.h"
#include "particles.h"
#include "neighbor.h"
#include "forces.h"
#include "boundary.h"

static void compute_deform_rate_tensor(particles *p_me, particles *pneighbor, 
    interact_params params);
static void particle_loop(particles *plist);

void deformation(particles *plist)
{
  if(neighbor==particle_based)
    particle_loop(plist);
}

static void compute_deform_rate_tensor(particles *p_me, particles *pneighbor, 
    interact_params params)
{
  if(pneighbor==NULL || p_me==NULL) return;

  particle *pa = p_me->p;
  particle *pb = pneighbor->p;

/*  if(pa->phase != pb->phase) return;*/
  /*if(pa->phase <2 || pb->phase <2) return;*/
  if(pa->phase ==1 || pb->phase ==1) return;

  if(p_me->p->id < 0 || pneighbor->p->id < 0){
    /*Morris'97: Set boundary velocity field*/
    boundary_set_vr(p_me, pneighbor, &params);
    /*Monaghan: Boundary force*/
    /*boundary_force(p_me, pneighbor, params);*/
  }
  /*Initialize tensor dv*/
  tensor dv;
  /* tensor dv is the green strain epsilon*/
  tensor rot;

  int i,j;
  for(i = 0; i < dim; i++)
    for(j = 0; j < dim; j++)
      (&(&dv.xx)[i].x)[j] = 0.;    

  vector dwdx = params.dwdx;
  vector vr = params.vr;
/*
  if(pa->id == 2000)
      printf("\n %f\t %f\t %2.8lf \t %f",vr.x,dwdx.x,pa->mass,pa->mu);*/

  for(i = 0; i < dim; i++){
    for(j = 0; j < dim; j++){
      (&(&dv.xx)[i].x)[i] += -((1./dim) * (&vr.x)[i]*(&dwdx.x)[i]) + ( (1./3.) * (&vr.x)[j]*(&dwdx.x)[j]);
      (&(&rot.xx)[i].x)[i] = 0;

      if(i!=j){
        (&(&dv.xx)[i].x)[j] = -0.5*((&vr.x)[i]*(&dwdx.x)[j] + (&vr.x)[j]*(&dwdx.x)[i]); /* deleted a +*/
        (&(&rot.xx)[i].x)[j] = -0.5*((&vr.x)[i]*(&dwdx.x)[j] - (&vr.x)[j]*(&dwdx.x)[i]);
      }
    }
  }
  for(i = 0; i < dim; i++){
    for(j = 0; j < dim; j++){
      (&(&(pa->stress).xx)[i].x)[j] += 2.*pa->mu * (pb->mass/pb->rho)*(&(&dv.xx)[i].x)[j];
      (&(&(pb->stress).xx)[i].x)[j] += 2.*pb->mu * (pa->mass/pa->rho)*(&(&dv.xx)[i].x)[j];


      (&(&(pa->vorticity).xx)[i].x)[j] += (pb->mass/pb->rho)*(&(&rot.xx)[i].x)[j];
      (&(&(pb->vorticity).xx)[i].x)[j] += - (pa->mass/pa->rho)*(&(&rot.xx)[i].x)[j];
    }
  }
}


static void particle_loop(particles *plist)
{  
  while(plist){ 
    neighbor_list *temp_list = plist->neighbors;
    while(temp_list){
      interact_params params;
      params.w = temp_list->w;
      params.xr = temp_list->xr;
      params.vr = temp_list->vr;
      params.dwdx = temp_list->dwdx;

      compute_deform_rate_tensor(plist, temp_list->p, params);

      temp_list = temp_list->next;
    }
    plist = plist->next;
  }  
}

void frame_indiff(particles *plist)
{
  while(plist){
    int i, j, k;

    particle * p = plist->p;
    plist = plist->next;
    if(p->phase < 2) continue;
    double indiff;
    for(i = 0; i < dim; i++){
      for(j = 0; j < dim; j++){
        indiff = 0;
        for(k =0; k<dim; k++) {

          indiff += (&(&(p->stress_tot).xx)[i].x)[k] * (&(&(p->vorticity).xx)[j].x)[k] 
            + (&(&(p->stress_tot).xx)[k].x)[j] * (&(&(p->vorticity).xx)[i].x)[k];

          /*          p->stress =  p->stress + S Omega + Omega S*/

        }
        (&(&(p->stress).xx)[i].x)[j] = (&(&(p->stress).xx)[i].x)[j] + indiff;
      }
    }

  }
}

