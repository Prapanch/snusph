#ifndef BOUNDARY_H_
#define BOUNDARY_H_

#include "sph.h"
#include "particles.h"

int bc_type;
void set_interior(particleData * pdata);
void boundary_condition(particleData * pdata, int check_bc);
void update_ghost_velocity(particleData *pdata, int tag);
void periodic_bc(particleData *pdata, int dir);

#endif
