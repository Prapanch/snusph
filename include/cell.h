#ifndef cell_id_ 

#define cell_id_

#include "sph.h"
void allocate_cells();

intvector id_to_coord(int);

int coord_to_id(intvector );

void particle_in_cell(particleData *);


#endif
