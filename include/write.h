#ifndef WRITE_H_
#define WRITE_H_

int write_single;
char *file_prefix;

simoutput * VTKgrid;
simoutput * VTKparticles;
simoutput * Tecplot;
simoutput * timefile;


void write(particleData *pdata, int step);
void write_particles_VTK_test(particleData *pdata);
boolean timefile_is_on;
boolean VTKparticles_is_on;
boolean VTKgrid_is_on;
boolean Tecplot_is_on;
void write_time_data(particleData *pdata);
#endif
