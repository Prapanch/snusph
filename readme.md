```
                            ___ ___ ___ _  _   ___ _                
    .---. .-. .-..-. .-.   |_ _/ __| _ \ || | | __| |_____ __ __     
   ( .-._)|  \| || | | |    | |\__ \  _/ __ | | _|| / _ \ V  V /     
  (_) \   |   | || | | |   |___|___/_| |_||_| |_| |_\___/\_/\_/      
  _  \ \  | |\  || | | |   / __| ___| |_ _____ _ _                   
 ( `-'  ) | | |)|| `-')|   \__ \/ _ \ \ V / -_) '_|                   
  `----'  /(  (_)`---(_)   |___/\___/_|\_/\___|_|                     
         (__)                                                        
```
Authors: Prapanch Nair, Gaurav Tomar, Michael Blank

## Installation
1. Install the [LIS library](http://www.ssisc.org/lis/index.en.html)
2. Install the [GNU scientific library](https://www.gnu.org/software/gsl//gsl.html) and [BLAS](http://www.netlib.org/blas/) libraries (Only for the parametric surface tension model. Can be skipped by commentin the corresponding include files and removing the requirement from cmake configuration file.)
3. Install [cmake](https://cmake.org)
4. In the `CMakeLists.txt` file, change the location of the libraries, include files
5. Create a project folder and type `cmake $PATH_TO_CODE`, where `$PATH_TO_CODE` is the code folder
with the `src` and `include` folder.
6. Cmake would have created the required make files. Now enter `make` or `make -j 4` (on a multiprocessor computer) to compile. 
7. Copy and input file from the examples folder to the project folder and run `./SPH 0 input` 


![](http://i.imgur.com/Vj7LWdE.gif =200x160)
